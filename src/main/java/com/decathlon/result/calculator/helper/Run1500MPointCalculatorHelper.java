package com.decathlon.result.calculator.helper;

import com.decathlon.result.calculator.helper.api.PointCalculatorHelper;

public class Run1500MPointCalculatorHelper implements PointCalculatorHelper {

    double a;
    double b;
    double c;

    public Run1500MPointCalculatorHelper() {
        this.a = 0.03768;
        this.b = 480;
        this.c = 1.85;
    }

    @Override
    public int calculatePoints(double result) {
        //this formula and constants come from http://en.wikipedia.org/wiki/Decathlon
        return (int)Math.round(a*Math.pow(b - result , c));
    }
}
