package com.decathlon.result.calculator.helper.api;

public interface PointCalculatorHelper {
    int calculatePoints(double result);
}
