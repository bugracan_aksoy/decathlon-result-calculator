package com.decathlon.result.calculator.helper;

import com.decathlon.result.calculator.helper.api.PointCalculatorHelper;

public class HighJumpPointCalculatorHelper implements PointCalculatorHelper {

    double a;
    double b;
    double c;

    public HighJumpPointCalculatorHelper() {
        this.a = 0.8465;
        this.b = 75;
        this.c = 1.42;
    }

    @Override
    public int calculatePoints(double result) {
        //this formula and constants come from http://en.wikipedia.org/wiki/Decathlon
        return (int)Math.round(a*Math.pow(result*100 - b , c));
    }
}
