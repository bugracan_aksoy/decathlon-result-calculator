package com.decathlon.result.calculator.helper;

import com.decathlon.result.calculator.helper.api.PointCalculatorHelper;

public class Run100MPointCalculatorHelper implements PointCalculatorHelper {

    double a;
    double b;
    double c;

    public Run100MPointCalculatorHelper() {
        this.a = 25.4347;
        this.b = 18;
        this.c = 1.81;
    }

    @Override
    public int calculatePoints(double result) {
        //this formula and constants come from http://en.wikipedia.org/wiki/Decathlon
        return (int)Math.round(a*Math.pow(b - result , c));
    }
}
