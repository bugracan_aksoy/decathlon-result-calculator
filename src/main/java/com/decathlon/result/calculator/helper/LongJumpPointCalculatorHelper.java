package com.decathlon.result.calculator.helper;

import com.decathlon.result.calculator.helper.api.PointCalculatorHelper;

public class LongJumpPointCalculatorHelper implements PointCalculatorHelper {

    double a;
    double b;
    double c;

    public LongJumpPointCalculatorHelper() {
        this.a = 0.14354;
        this.b = 220;
        this.c = 1.4;
    }

    @Override
    public int calculatePoints(double result) {
        //this formula and constants come from http://en.wikipedia.org/wiki/Decathlon
        return (int)Math.round(a*Math.pow(result*100 - b, c));
    }
}
