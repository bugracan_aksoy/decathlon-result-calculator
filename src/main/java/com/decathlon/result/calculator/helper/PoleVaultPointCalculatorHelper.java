package com.decathlon.result.calculator.helper;

import com.decathlon.result.calculator.helper.api.PointCalculatorHelper;

public class PoleVaultPointCalculatorHelper implements PointCalculatorHelper {

    double a;
    double b;
    double c;

    public PoleVaultPointCalculatorHelper() {
        this.a = 0.2797;
        this.b = 100;
        this.c = 1.35;
    }

    @Override
    public int calculatePoints(double result) {
        //this formula and constants come from http://en.wikipedia.org/wiki/Decathlon
        return (int)Math.round(a*Math.pow(result*100 - b , c));
    }
}
