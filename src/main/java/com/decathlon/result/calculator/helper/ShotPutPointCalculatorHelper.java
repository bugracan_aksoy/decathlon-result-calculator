package com.decathlon.result.calculator.helper;

import com.decathlon.result.calculator.helper.api.PointCalculatorHelper;

public class ShotPutPointCalculatorHelper implements PointCalculatorHelper {

    double a;
    double b;
    double c;

    public ShotPutPointCalculatorHelper() {
        this.a = 51.39;
        this.b = 1.5;
        this.c = 1.05;
    }

    @Override
    public int calculatePoints(double result) {
        //this formula and constants come from http://en.wikipedia.org/wiki/Decathlon
        return (int)Math.round(a*Math.pow(result - b , c));
    }
}
