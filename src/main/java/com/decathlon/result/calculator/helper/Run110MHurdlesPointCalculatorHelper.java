package com.decathlon.result.calculator.helper;

import com.decathlon.result.calculator.helper.api.PointCalculatorHelper;

public class Run110MHurdlesPointCalculatorHelper implements PointCalculatorHelper {

    double a;
    double b;
    double c;

    public Run110MHurdlesPointCalculatorHelper() {
        this.a = 5.74352;
        this.b = 28.5;
        this.c = 1.92;
    }

    @Override
    public int calculatePoints(double result) {
        //this formula and constants come from http://en.wikipedia.org/wiki/Decathlon
        return (int)Math.round(a*Math.pow(b - result , c));
    }
}
