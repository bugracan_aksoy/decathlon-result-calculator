package com.decathlon.result.calculator.helper;

import com.decathlon.result.calculator.helper.api.PointCalculatorHelper;

public class JavelinThrowPointCalculatorHelper implements PointCalculatorHelper {

    double a;
    double b;
    double c;

    public JavelinThrowPointCalculatorHelper() {
        this.a = 10.14;
        this.b = 7;
        this.c = 1.08;
    }

    @Override
    public int calculatePoints(double result) {
        //this formula and constants come from http://en.wikipedia.org/wiki/Decathlon
        return (int)Math.round(a*Math.pow(result - b , c));
    }
}
