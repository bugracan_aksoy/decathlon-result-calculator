package com.decathlon.result.calculator.helper;

import com.decathlon.result.calculator.helper.api.PointCalculatorHelper;

public class DiscusThrowPointCalculatorHelper implements PointCalculatorHelper {

    double a;
    double b;
    double c;

    public DiscusThrowPointCalculatorHelper() {
        this.a = 12.91;
        this.b = 4;
        this.c = 1.1;
    }

    @Override
    public int calculatePoints(double result) {
        //this formula and constants come from http://en.wikipedia.org/wiki/Decathlon
        return (int)Math.round(a*Math.pow(result - b , c));
    }
}
