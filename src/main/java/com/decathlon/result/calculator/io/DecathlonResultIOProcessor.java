package com.decathlon.result.calculator.io;

import com.decathlon.result.calculator.data.AthleteDetails;
import com.decathlon.result.calculator.data.DecathlonResult;
import com.decathlon.result.calculator.data.DecathlonResultWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class DecathlonResultIOProcessor {

    //get results data from csv and return it in application data format
    public List<DecathlonResult> readResultsFromCSVFile() {
        String fileName = "results.csv";
        InputStream inputStream = getFileFromResourceAsStream(fileName);
        return parseResults(inputStream);
    }

    //write final results after calculations to xml file
    public void writeResultsToXMLFile(List<DecathlonResult> decathlonResults) throws JAXBException {

        File file = new File("finalResults.xml");
        DecathlonResultWrapper decathlonResultWrapper = new DecathlonResultWrapper(decathlonResults);

        JAXBContext jaxbContext = JAXBContext.newInstance(DecathlonResultWrapper.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(decathlonResultWrapper, file);
        jaxbMarshaller.marshal(decathlonResultWrapper, System.out);


    }

    private InputStream getFileFromResourceAsStream(String fileName) {

        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);

        if (inputStream == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            return inputStream;
        }

    }

    private List<DecathlonResult> parseResults(InputStream is) {

        try (InputStreamReader streamReader =
                     new InputStreamReader(is, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(streamReader)) {

            String line;
            List<DecathlonResult> decathlonResultList = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                if (!line.equals("")) {
                    //parse result data with splitting it
                    String[] results = line.split(";");
                    DecathlonResult decathlonResult = new DecathlonResult();
                    AthleteDetails athleteDetails = new AthleteDetails(results[0]);
                    decathlonResult.setAthleteDetails(athleteDetails);
                    decathlonResult.setRun100M(Double.parseDouble(results[1]));
                    decathlonResult.setLongJump(Double.parseDouble(results[2]));
                    decathlonResult.setShotPut(Double.parseDouble(results[3]));
                    decathlonResult.setHighJump(Double.parseDouble(results[4]));
                    decathlonResult.setRun400M(Double.parseDouble(results[5]));
                    decathlonResult.setRun110MHurdles(Double.parseDouble(results[6]));
                    decathlonResult.setDiscusThrow(Double.parseDouble(results[7]));
                    decathlonResult.setPoleVault(Double.parseDouble(results[8]));
                    decathlonResult.setJavelinThrow(Double.parseDouble(results[9]));
                    decathlonResult.setRun1500M((results[10]));

                    decathlonResultList.add(decathlonResult);
                }
            }
            return decathlonResultList;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
