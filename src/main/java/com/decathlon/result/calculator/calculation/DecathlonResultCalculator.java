package com.decathlon.result.calculator.calculation;

import com.decathlon.result.calculator.data.DecathlonResult;
import com.decathlon.result.calculator.helper.*;

import java.util.List;

public class DecathlonResultCalculator {


    public void calculateResult(DecathlonResult decathlonResult) {
        int totalPoints = 0;
        totalPoints += new Run100MPointCalculatorHelper().calculatePoints(decathlonResult.getRun100M());
        totalPoints += new LongJumpPointCalculatorHelper().calculatePoints(decathlonResult.getLongJump());
        totalPoints += new ShotPutPointCalculatorHelper().calculatePoints(decathlonResult.getShotPut());
        totalPoints += new HighJumpPointCalculatorHelper().calculatePoints(decathlonResult.getHighJump());
        totalPoints += new Run400MPointCalculatorHelper().calculatePoints(decathlonResult.getRun400M());
        totalPoints += new Run110MHurdlesPointCalculatorHelper().calculatePoints(decathlonResult.getRun110MHurdles());
        totalPoints += new DiscusThrowPointCalculatorHelper().calculatePoints(decathlonResult.getDiscusThrow());
        totalPoints += new PoleVaultPointCalculatorHelper().calculatePoints(decathlonResult.getPoleVault());
        totalPoints += new JavelinThrowPointCalculatorHelper().calculatePoints(decathlonResult.getJavelinThrow());
        totalPoints += new Run1500MPointCalculatorHelper().calculatePoints(parse1500MResult(decathlonResult.getRun1500M()));
        decathlonResult.setTotalPoints(totalPoints);
    }

    public void determineRankings(List<DecathlonResult> decathlonResultList) {
        int duplicatePointCount = 0;
        for (int i = 0; i <= decathlonResultList.size()-1; i++) {
            if(i<decathlonResultList.size()-1 && decathlonResultList.get(i).getTotalPoints() == decathlonResultList.get(i + 1).getTotalPoints()){
                duplicatePointCount+=1;
            }else if(duplicatePointCount>0){
                String duplicateRank = prepareDuplicateRank(duplicatePointCount, i);
                for(int j = duplicatePointCount; j>=0; j--){
                    decathlonResultList.get(i-j).setRanking(String.valueOf(duplicateRank));
                }
                duplicatePointCount=0;
            } else if(duplicatePointCount == 0){
                decathlonResultList.get(i).setRanking(String.valueOf(i+1));
            }
        }
    }

    private String prepareDuplicateRank(int duplicatePointCount, int index){
        String duplicateRank ="";
        for(int k = duplicatePointCount; k>=0; k--){
            duplicateRank += String.valueOf((index-k)+1);
            if(k!=0)
                duplicateRank += "-";
        }
        return duplicateRank;
    }

    private double parse1500MResult(String run1500MResult) {
        String[] run1500MResultDivided = run1500MResult.split(":");
        int minute = Integer.parseInt(run1500MResultDivided[0]);
        double second = Double.parseDouble(run1500MResultDivided[1]);
        double result = (minute * 60) + second;
        return result;
    }
}
