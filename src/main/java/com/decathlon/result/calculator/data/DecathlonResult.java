package com.decathlon.result.calculator.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlSeeAlso({AthleteDetails.class})
public class DecathlonResult {

    private AthleteDetails athleteDetails;
    private double run100M;
    private double longJump;
    private double shotPut;
    private double highJump;
    private double run400M;
    private double run110MHurdles;
    private double discusThrow;
    private double poleVault;
    private double javelinThrow;
    private String run1500M;

    private int totalPoints;
    private String ranking;

    public AthleteDetails getAthleteDetails() {
        return athleteDetails;
    }

    @XmlElement
    public void setAthleteDetails(AthleteDetails athleteDetails) {
        this.athleteDetails = athleteDetails;
    }

    public double getRun100M() {
        return run100M;
    }

    @XmlElement
    public void setRun100M(double run100M) {
        this.run100M = run100M;
    }

    public double getLongJump() {
        return longJump;
    }

    @XmlElement
    public void setLongJump(double longJump) {
        this.longJump = longJump;
    }

    public double getShotPut() {
        return shotPut;
    }

    @XmlElement
    public void setShotPut(double shotPut) {
        this.shotPut = shotPut;
    }

    public double getHighJump() {
        return highJump;
    }

    @XmlElement
    public void setHighJump(double highJump) {
        this.highJump = highJump;
    }

    public double getRun400M() {
        return run400M;
    }

    @XmlElement
    public void setRun400M(double run400M) {
        this.run400M = run400M;
    }

    public double getRun110MHurdles() {
        return run110MHurdles;
    }

    @XmlElement
    public void setRun110MHurdles(double run110MHurdles) {
        this.run110MHurdles = run110MHurdles;
    }

    public double getDiscusThrow() {
        return discusThrow;
    }

    @XmlElement
    public void setDiscusThrow(double discusThrow) {
        this.discusThrow = discusThrow;
    }

    public double getPoleVault() {
        return poleVault;
    }

    @XmlElement
    public void setPoleVault(double poleVault) {
        this.poleVault = poleVault;
    }

    public double getJavelinThrow() {
        return javelinThrow;
    }

    @XmlElement
    public void setJavelinThrow(double javelinThrow) {
        this.javelinThrow = javelinThrow;
    }

    public String getRun1500M() {
        return run1500M;
    }

    @XmlElement
    public void setRun1500M(String run1500M) {
        this.run1500M = run1500M;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    @XmlElement
    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getRanking() {
        return ranking;
    }

    @XmlElement
    public void setRanking(String ranking) {
        this.ranking = ranking;
    }
}
