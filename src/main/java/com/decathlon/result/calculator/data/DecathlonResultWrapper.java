package com.decathlon.result.calculator.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

//this class is created to write out the xml file
@XmlRootElement
@XmlSeeAlso({DecathlonResult.class})
public class DecathlonResultWrapper {


    List<DecathlonResult> decathlonResultsList;

    public DecathlonResultWrapper() {
    }

    public DecathlonResultWrapper(List<DecathlonResult> decathlonResultsList) {
        this.decathlonResultsList = decathlonResultsList;
    }

    public List<DecathlonResult> getDecathlonResultsList() {
        return decathlonResultsList;
    }
    @XmlElement
    public void setDecathlonResultsList(List<DecathlonResult> decathlonResultsList) {
        this.decathlonResultsList = decathlonResultsList;
    }
}
