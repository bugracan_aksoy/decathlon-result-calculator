package com.decathlon.result.calculator;

import com.decathlon.result.calculator.calculation.DecathlonResultCalculator;
import com.decathlon.result.calculator.data.DecathlonResult;
import com.decathlon.result.calculator.io.DecathlonResultIOProcessor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class DecathlonResultInitializer {

    private static List<DecathlonResult> decathlonResults = new ArrayList<>();
    DecathlonResultCalculator decathlonResultCalculator = new DecathlonResultCalculator();
    DecathlonResultIOProcessor decathlonResultIOProcessor = new DecathlonResultIOProcessor();
    static DecathlonResultInitializer app = new DecathlonResultInitializer();

    public static void main(String[] args) {
        app.readDecathlonResultsAndRank();
    }

    public void readDecathlonResultsAndRank(){
        try {
            //fetch result data
            decathlonResults = app.decathlonResultIOProcessor.readResultsFromCSVFile();

            //calculate total points of all athletes
            for(DecathlonResult decathlonResult: decathlonResults){
                app.decathlonResultCalculator.calculateResult(decathlonResult);
            }

            //sort all results according to total points
            decathlonResults.sort(Comparator.comparing(DecathlonResult::getTotalPoints).reversed());

            //determine the exact ranking of results
            app.decathlonResultCalculator.determineRankings(decathlonResults);

            //write ranked results to xml file
            app.decathlonResultIOProcessor.writeResultsToXMLFile(decathlonResults);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

}

