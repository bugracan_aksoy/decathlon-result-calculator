package com.decathlon.result.calculator.test;


import com.decathlon.result.calculator.calculation.DecathlonResultCalculator;
import com.decathlon.result.calculator.DecathlonResultInitializer;
import com.decathlon.result.calculator.data.DecathlonResult;
import com.decathlon.result.calculator.data.DecathlonResultWrapper;
import com.decathlon.result.calculator.io.DecathlonResultIOProcessor;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DecathlonResultInitializerTest {


    @Test
    public void fetchResultData() {
        //check the fetching process of csv
        List<DecathlonResult> decathlonResultList = new DecathlonResultIOProcessor().readResultsFromCSVFile();
        assertNotNull(decathlonResultList);
    }

    @Test
    public void calculatePointsOfResultData() {
        //maximum results to achieve 10000 points - 1000 points each
        DecathlonResult decathlonResult = new DecathlonResult();
        decathlonResult.setRun100M(10.362);
        decathlonResult.setLongJump(7.76);
        decathlonResult.setShotPut(18.4);
        decathlonResult.setHighJump(2.20);
        decathlonResult.setRun400M(46.17);
        decathlonResult.setRun110MHurdles(13.8);
        decathlonResult.setDiscusThrow(56.17);
        decathlonResult.setPoleVault(5.28);
        decathlonResult.setJavelinThrow(77.19);
        decathlonResult.setRun1500M("3:53.79");

        new DecathlonResultCalculator().calculateResult(decathlonResult);
        assertEquals(decathlonResult.getTotalPoints(),10000);
        assertNull(decathlonResult.getRanking());
    }

    @Test
    public void rankResults() {
        //check ranking process
        List<DecathlonResult> decathlonResultList = new DecathlonResultIOProcessor().readResultsFromCSVFile();
        assertNotNull(decathlonResultList);
        new DecathlonResultCalculator().determineRankings(decathlonResultList);
        for(DecathlonResult decathlonResult: decathlonResultList){
            assertNotNull(decathlonResult.getRanking());
        }
    }

    @Test
    public void displayResult() throws JAXBException {
        //check the output file
        List<DecathlonResult> decathlonResultList = new DecathlonResultIOProcessor().readResultsFromCSVFile();
        assertNotNull(decathlonResultList);

        new DecathlonResultIOProcessor().writeResultsToXMLFile(decathlonResultList);
        File file = new File("finalResults.xml");
        assertNotNull(file);
    }

    @Test
    public void integrationTestOfSystem() throws JAXBException {
        //check all system combined
        new DecathlonResultInitializer().readDecathlonResultsAndRank();
        JAXBContext jaxbContext = JAXBContext.newInstance(DecathlonResultWrapper.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();


        DecathlonResultWrapper decathlonResultWrapper = (DecathlonResultWrapper) jaxbUnmarshaller.unmarshal( new File("finalResults.xml") );
        assertNotNull(decathlonResultWrapper);

        List<DecathlonResult> decathlonResultList = decathlonResultWrapper.getDecathlonResultsList();
        assertNotNull(decathlonResultList);

        for(DecathlonResult decathlonResult: decathlonResultList){
            assertNotNull(decathlonResult.getRanking());
        }


    }



}
